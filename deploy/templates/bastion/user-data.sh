#!/bin/bash

sudo yum update -y
sudo amazon-linux-extras install -t docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user